package com.example.igorx.cookietestproject.adapters;


import android.content.Context;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.igorx.cookietestproject.PostsModelLab;
import com.example.igorx.cookietestproject.R;
import com.example.igorx.cookietestproject.interfaces.ShowDetailInterface;
import com.example.igorx.cookietestproject.models.Datum;
import com.example.igorx.cookietestproject.models.LowResolution;
import com.example.igorx.cookietestproject.models.PostsModel;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

public class PostRecyclerAdapter extends RecyclerView.Adapter<PostRecyclerAdapter.ViewHolder> {

    private ArrayList<LowResolution> lowResolutionImages;
    private PostsModel postsModel;
    private ShowDetailInterface showDetailInterface;
    private Context context;

    public PostRecyclerAdapter(Context context, ArrayList<LowResolution> lowResolutionImages,ShowDetailInterface showDetailInterface ) {
        this.context=context;
        this.lowResolutionImages=lowResolutionImages;
        this.showDetailInterface=showDetailInterface;
       // this.postsModel=postsModel;
       // this.lowResolutionImages=getLowResulutionImage();
    }

    private ArrayList<LowResolution> getLowResulutionImage() {
        ArrayList<LowResolution> images = new ArrayList<>();
        for (Datum datumModel : postsModel.getData()) {
            images.add(datumModel.getImages().getLowResolution());
        }
        return images;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.post_grid_item_adapter, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {
        holder.tvCountLikes.setText("" + PostsModelLab.getInstance().getPostsModel().getData().get(position).getLikes().getCount());
        Picasso.with(context).load(lowResolutionImages.get(position).getUrl()).placeholder(R.drawable.tovar_defoult_ico_small).fit().into(holder.imFoto);
        holder.imFoto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showDetailInterface.showDetail(position);
            }
        });
    }

    @Override
    public long getItemId(int position) {
        return super.getItemId(position);
    }

    @Override
    public int getItemCount() {
        return lowResolutionImages.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        ImageView imFoto;
        TextView tvCountLikes;
        public ViewHolder(View itemView) {
            super(itemView);
            imFoto = (ImageView) itemView.findViewById(R.id.ivFoto);
            tvCountLikes = (TextView) itemView.findViewById(R.id.tvLikes);
        }
    }
    @Override
    public void onAttachedToRecyclerView(RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
    }
}
