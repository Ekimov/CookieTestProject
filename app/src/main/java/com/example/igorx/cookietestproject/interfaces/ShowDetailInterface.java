package com.example.igorx.cookietestproject.interfaces;


public interface ShowDetailInterface {
    void showDetail(int position);
}
