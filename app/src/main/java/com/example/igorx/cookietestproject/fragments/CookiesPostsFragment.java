package com.example.igorx.cookietestproject.fragments;


import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.example.igorx.cookietestproject.AppConst;
import com.example.igorx.cookietestproject.MainActivity;
import com.example.igorx.cookietestproject.PostsModelLab;
import com.example.igorx.cookietestproject.R;
import com.example.igorx.cookietestproject.Service;
import com.example.igorx.cookietestproject.adapters.PostRecyclerAdapter;
import com.example.igorx.cookietestproject.interfaces.ShowDetailInterface;
import com.example.igorx.cookietestproject.models.Datum;
import com.example.igorx.cookietestproject.models.LowResolution;
import com.example.igorx.cookietestproject.models.PostsModel;

import java.util.ArrayList;

import me.zhanghai.android.materialprogressbar.MaterialProgressBar;
import retrofit.Call;
import retrofit.Callback;
import retrofit.Response;
import retrofit.Retrofit;

public class CookiesPostsFragment extends Fragment implements ShowDetailInterface{

    private RecyclerView rvPosts;
    private ProgressBar pbPosts;
    private PostRecyclerAdapter adapter;
    private ArrayList<LowResolution> lowResolutionsImages;
    private RelativeLayout rlNoConnection;
    private Button btnNoConnection;
    private TextView tvNothingFound;


    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.cookies_posts_fragment, container, false);
        setHasOptionsMenu(true);
        ((MainActivity) getActivity()).getSupportActionBar().setDisplayHomeAsUpEnabled(false);
        initView(view);
        if (lowResolutionsImages == null) {
            getPostsRequest("rookypro");
        } else {
            showGridPosts();
        }
        return view;
    }

    private void initView(View view) {
        rvPosts = (RecyclerView) view.findViewById(R.id.rvPosts);
        pbPosts = (ProgressBar) view.findViewById(R.id.pbPosts);
        rlNoConnection= (RelativeLayout) view.findViewById(R.id.rlNoConnection);
        tvNothingFound= (TextView) view.findViewById(R.id.tvNothingFound);
        btnNoConnection= (Button) view.findViewById(R.id.btnNoConnection);
        btnNoConnection.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               // startAnimation(rlNoConnection,pbPosts,100);
                rlNoConnection.setVisibility(View.GONE);
                getPostsRequest("rookypro");
            }
        });
    }
    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater menuInflater) {
        super.onCreateOptionsMenu(menu, menuInflater);
        menuInflater.inflate(R.menu.menu, menu);
        final MenuItem searchItem = menu.findItem(R.id.menu_item_search);
        final SearchView searchView = (SearchView) searchItem.getActionView();
        searchView.setSubmitButtonEnabled(true);
        searchView.setOnQueryTextListener
                (new SearchView.OnQueryTextListener() {
                    @Override
                    public boolean onQueryTextSubmit(String s) {
                        searchItem.collapseActionView();
                        hideKeyBoard(searchView);
                        tvNothingFound.setVisibility(View.GONE);
                        getPostsRequest(s);
                        PostsModelLab.startAnimation(rvPosts,pbPosts,500);
                        return true;
                    }
                    @Override
                    public boolean onQueryTextChange(String s) {
                        return false;
                    }
                });
    }


    private void getPostsRequest(String tagName) {
        pbPosts.setVisibility(View.VISIBLE);
        Call<PostsModel> postCall = Service.createTokenService().getPosts(tagName, AppConst.API_KEY);
        postCall.enqueue(new Callback<PostsModel>() {
            @Override
            public void onResponse(Response<PostsModel> response, Retrofit retrofit) {
                //pbPosts.setVisibility(View.GONE);
                if (response.isSuccess()) {
                    lowResolutionsImages = new ArrayList();
                    PostsModel postsModel = response.body();
                    PostsModelLab.getInstance().setPostsModel(postsModel);
                    for (Datum datumModel : postsModel.getData()) {
                        lowResolutionsImages.add(datumModel.getImages().getLowResolution());
                    }
                    showGridPosts();
                    return;
                }
                rlNoConnection.setVisibility(View.VISIBLE);
            }

            @Override
            public void onFailure(Throwable t) {
                pbPosts.setVisibility(View.GONE);
                rlNoConnection.setVisibility(View.VISIBLE);
                //startAnimation(pbPosts,rlNoConnection);
            }
        });
    }

    private void showGridPosts() {
        if(lowResolutionsImages.isEmpty()){
            PostsModelLab.startAnimation(pbPosts,tvNothingFound,500);
           // tvNothingFound.setVisibility(View.VISIBLE);
            return;
        }
        tvNothingFound.setVisibility(View.GONE);
        PostsModelLab.startAnimation(pbPosts,rvPosts,500);
        GridLayoutManager gridLayoutManager=new GridLayoutManager(getActivity(),2);
        rvPosts.setLayoutManager(gridLayoutManager);
        adapter=new PostRecyclerAdapter(getActivity(),lowResolutionsImages,this);
        rvPosts.setAdapter(adapter);
    }

    @Override
    public void showDetail(int position) {
        FragmentTransaction ft = getActivity().getSupportFragmentManager().beginTransaction();
        ft.replace(R.id.fragContain,DetailCookiesPostFragment.newInstance(position));
        ft.addToBackStack(CookiesPostsFragment.class.getCanonicalName());
        ft.commit();
    }



    private void hideKeyBoard(View view){
        InputMethodManager imm = (InputMethodManager)getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }
}
