package com.example.igorx.cookietestproject.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.igorx.cookietestproject.R;
import com.example.igorx.cookietestproject.models.comments.Datum;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by IgorX on 14.05.2016.
 */
public class CommentRecyclerAdapter extends RecyclerView.Adapter<CommentRecyclerAdapter.ViewHolder> {
    private List<Datum> dataComments;
    private Context context;

    public CommentRecyclerAdapter(Context context, List<Datum> dataComments) {
        this.dataComments = dataComments;
        this.context = context;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_comment_adapter, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        Datum dataComment = dataComments.get(position);
        Picasso.with(context).load(dataComment.getFrom().getProfilePicture()).placeholder(R.drawable.tovar_defoult_ico_small).fit().into(holder.imPhoto);
        holder.tvName.setText(dataComment.getFrom().getFullName());
        holder.tvComments.setText(dataComment.getText());
    }

    @Override
    public long getItemId(int position) {
        return super.getItemId(position);
    }

    @Override
    public int getItemCount() {
        return dataComments.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        ImageView imPhoto;
        TextView tvName, tvComments;

        public ViewHolder(View itemView) {
            super(itemView);
            imPhoto = (ImageView) itemView.findViewById(R.id.ivPhotoPerson);
            tvName = (TextView) itemView.findViewById(R.id.tvName);
            tvComments = (TextView) itemView.findViewById(R.id.tvComments);
        }
    }

    @Override
    public void onAttachedToRecyclerView(RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
    }
}
