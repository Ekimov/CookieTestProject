package com.example.igorx.cookietestproject;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.LinearLayout;

public class SplashActivity extends AppCompatActivity {

    private LinearLayout splashLogo;
    private Animation anim;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        splashLogo=(LinearLayout)findViewById(R.id.splashLogo);
        anim = AnimationUtils.loadAnimation(getBaseContext(), R.anim.splash_anim);
        splashLogo.setAnimation(anim);
        timeSplashScreen();
    }

    private void timeSplashScreen(){
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                Intent intent = new Intent(SplashActivity.this, MainActivity.class);
                startActivity(intent);
                finish();
            }
        }, 1 * 1800);
    }
}