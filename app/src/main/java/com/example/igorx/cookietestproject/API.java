package com.example.igorx.cookietestproject;


import com.example.igorx.cookietestproject.models.PostsModel;
import com.example.igorx.cookietestproject.models.comments.CommentModel;
import com.squareup.okhttp.ResponseBody;

import java.util.List;

import retrofit.Call;
import retrofit.Response;
import retrofit.http.GET;
import retrofit.http.Path;
import retrofit.http.Query;

public interface API {

//    @GET("/v1/tags/search")
//    Call<ResponseBody> getPosts(@Query("q")String query, @Query("access_token") String accessToken);

    @GET("/v1/tags/{tag-name}/media/recent")
    Call<PostsModel> getPosts(@Path("tag-name") String tagName, @Query("access_token") String accessToken );

    @GET("/v1/media/{media-id}/comments")
    Call<CommentModel> getComments(@Path("media-id") String mediaId, @Query("access_token") String accessToken );

}
