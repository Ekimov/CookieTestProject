package com.example.igorx.cookietestproject.fragments;


import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.example.igorx.cookietestproject.AppConst;
import com.example.igorx.cookietestproject.MainActivity;
import com.example.igorx.cookietestproject.PostsModelLab;
import com.example.igorx.cookietestproject.R;
import com.example.igorx.cookietestproject.Service;
import com.example.igorx.cookietestproject.adapters.CommentRecyclerAdapter;
import com.example.igorx.cookietestproject.models.PostsModel;
import com.example.igorx.cookietestproject.models.comments.CommentModel;
import com.example.igorx.cookietestproject.models.comments.Datum;
import com.squareup.okhttp.ResponseBody;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import me.zhanghai.android.materialprogressbar.MaterialProgressBar;
import retrofit.Call;
import retrofit.Callback;
import retrofit.Response;
import retrofit.Retrofit;

public class DetailCookiesPostFragment extends Fragment {

    private static final String SELECT_POSITION = "POSITION";
    private int mPosition;
    private ImageView imFoto;
    private RecyclerView rvComments;
    private CommentRecyclerAdapter commentAdapter;
    private TextView  tvLikes, tvCaption;
    private ProgressBar pbComments;
    private List<Datum> dataComments;

    public static DetailCookiesPostFragment newInstance(int position) {
        DetailCookiesPostFragment fragment = new DetailCookiesPostFragment();
        Bundle bundle = new Bundle();
        bundle.putInt(SELECT_POSITION, position);
        fragment.setArguments(bundle);
        return fragment;
    }
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        mPosition = getArguments().getInt(SELECT_POSITION);
        ((MainActivity) getActivity()).getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        setHasOptionsMenu(true);
        setRetainInstance(true);
        String imageFoto = PostsModelLab.getInstance().getPostsModel().getData().get(mPosition).getImages().getStandardResolution().getUrl();
        View view = inflater.inflate(R.layout.detail_post_fragment, container, false);
        initView(view);
        if(dataComments==null) {
            getCommentsRequest();
        }
        else {
            showComments();
        }
        if (imageFoto != null) {
            Picasso.with(getActivity()).load(imageFoto).placeholder(R.drawable.tovar_defoult_ico_small).into(imFoto);
        }
        tvLikes.setText("" + PostsModelLab.getInstance().getPostsModel().getData().get(mPosition).getLikes().getCount());
        tvCaption.setText(PostsModelLab.getInstance().getPostsModel().getData().get(mPosition).getCaption().getText());
        return view;
    }
   private void getCommentsRequest(){
       pbComments.setVisibility(View.VISIBLE);
       Call<CommentModel> commentsCall = Service.createTokenService().getComments(PostsModelLab.getInstance().getPostsModel().getData().get(mPosition).getId(),AppConst.API_KEY);
       commentsCall.enqueue(new Callback<CommentModel>() {
           @Override
           public void onResponse(Response<CommentModel> response, Retrofit retrofit) {
               dataComments=response.body().getData();
               if(dataComments!=null) {
                   showComments();
               }
           }

           @Override
           public void onFailure(Throwable t) {
               pbComments.setVisibility(View.GONE);
               t.printStackTrace();
       }
       });
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
              getActivity().onBackPressed();
        }
        return super.onOptionsItemSelected(item);
    }

    private void showComments() {
        pbComments.setVisibility(View.GONE);
        rvComments.setVisibility(View.VISIBLE);
        LinearLayoutManager llm = new LinearLayoutManager(getActivity());
        rvComments.setLayoutManager(llm);
        commentAdapter=new CommentRecyclerAdapter(getActivity(),dataComments);
        rvComments.setAdapter(commentAdapter);
    }

    private void initView(View view) {
        imFoto = (ImageView) view.findViewById(R.id.ivFoto);
        tvLikes = (TextView) view.findViewById(R.id.tvLikes);
        tvCaption = (TextView) view.findViewById(R.id.tvCaption);
        rvComments = (RecyclerView) view.findViewById(R.id.rvComments);
        pbComments = (ProgressBar) view.findViewById(R.id.pbComments);
    }
}
