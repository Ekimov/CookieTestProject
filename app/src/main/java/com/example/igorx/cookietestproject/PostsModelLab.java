package com.example.igorx.cookietestproject;


import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.view.View;

import com.example.igorx.cookietestproject.models.PostsModel;

public class PostsModelLab {
    private PostsModel postsModel;
    private static PostsModelLab postsModelLab;
    public static PostsModelLab getInstance() {
        if (postsModelLab == null) {
            postsModelLab = new PostsModelLab();
        }
        return postsModelLab;
    }

    private PostsModelLab() {
    }

    public void setPostsModel(PostsModel postsModel) {
        this.postsModel = postsModel;
    }

    public PostsModel getPostsModel() {
        return postsModel;
    }
    public static void startAnimation(final View mView1, final View mView2, final int duration ) {
        mView2.setAlpha(0f);
        mView2.setVisibility(View.VISIBLE);
        mView1.animate()
                .alpha(0f)
                .setDuration(duration)
                .setListener(new AnimatorListenerAdapter() {
                    @Override
                    public void onAnimationEnd(Animator animation) {
                        mView1.setVisibility(View.GONE);
                        mView2.animate()
                                .alpha(1f)
                                .setDuration(duration)
                                .setListener(null);
                    }
                });
    }
}
