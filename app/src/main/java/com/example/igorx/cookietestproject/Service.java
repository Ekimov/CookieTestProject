package com.example.igorx.cookietestproject;

import retrofit.GsonConverterFactory;
import retrofit.Retrofit;

public class Service{

    public static API createTokenService(){
        return getRetrofit().create(API.class);
    }

    private static Retrofit getRetrofit() {
        return new Retrofit.Builder()
                .baseUrl(AppConst.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
    }
}
